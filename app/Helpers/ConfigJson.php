<?php

namespace app\Helpers;
use App\Interfaces\ConfigJsonInterface;

class ConfigJson implements ConfigJsonInterface{

    protected $configFile;
    protected $configPath;
    protected $configJson;
    protected $enable;

    
    /*  
      public function isEnable():boolean{
        return $this->enable;  
    }
    */
    
    /** @param string configFile
      * @return $this
      */
      public function setConfigFile($configFile){
        $this->configFile = $configFile; 
       return $this;  
   }

   /** @return string */
    public function getConfigFile(){
        return $this->getConfigPath().'/'.$this->configFile;  
    }

     /** @param string configPath
     * @return $this
     */
    public function setConfigPath($configPath){
       $this->configPath = base_path().$configPath; 
      return $this;  
  }

  /** @return string */
   public function getConfigPath(){
       return $this->configPath;  
   }

    /** @param string enable
      * @return $this
      */
      public function setEnable($enable){
        $this->enable=$enable;
        return  $this;  
   }
    /** @param string configJson
      * @return $this
      */
      public function setConfigJson($configJson){
        $this->configJson=$configJson;
        return  $this;  
   }

   /** @return string */
    public function getConfigJson(){
        return $this->configJson;  
    }

}