<?php

namespace app\Helpers;
use App\Helpers\ConfigJson; 

use Exception;
class EndpointConfig extends ConfigJson{

    const CONFIG_FILE = 'endpoint.json';

    protected $param;
    private $name;
    private $endpoint;
    private $method;
    protected $baseConexion;
    

    public function __construct($name = 'cancel_controller', $account = 'principal', $configPath = null, $configFile = null, $configFileConexion = null, $configPathConexion = null){
        $this->setConfigPath($configPath ?? self::CONFIG_PATH)
             ->setConfigFile($configFile ?? self::CONFIG_FILE)
             ->setConfigJson(json_decode(file_get_contents($this->getConfigFile()), true)) 
             ->setName($name);
        $this->baseConexion = new UrlConfigVtex($account, $configFileConexion, $configPathConexion);      
    }

    /** @param string name
     * @return $this
     */
    public function setName($name){

        $config = '';
        $config = array_filter($this->getConfigJson(), function($nodo) use($name) {return $nodo['name'] == $name; } );
        if(count($config,1)  == 0){
            throw new Exception('Enpoint not config found');
        }

        $config=reset($config);
        $this->setEndpoint($config['endpoint'])
             ->setEnable($config['enable']);

        $this->name=$name;
        return  $this;  
   }
 
   /** @return string */
    public function getName(){
        return $this->name;  
    }
        
    /** @param string endpoint
     * @return $this
     */
    public function setEndpoint($endpoint){
        $this->endpoint=$endpoint;
        return  $this;  
   }
 

   /** @return string */
    public function getEndpoint($params = [], $posicion = 1){
        $result = count($params) >= 1 ? $this->addParamUrl($params,$posicion).$this->addParamToken().$this->addParamAppKey() : $this->baseConexion->getUrl().$this->endpoint; 
        return $result;
    }

    /** @param string baseConexion
     * @return $this
     */
    public function setBaseConexion($baseConexion){
        $this->baseConexion=$baseConexion;
        return $this;  
   }

    /** @param string baseConexion
     * @return $this
     */
    public function getBaseConexion(){
        $this->baseConexion;
        return $this;  
   }

       /** @param string[] param
     * @return $this
     */
    public function setParam($param){
        $this->param=$param;
        return $this;  
   }

    /** @param string[] param
     * @return $this
     */
    public function getParam(){
        $this->param;
        return $this;  
   }

   public function addParamUrl($params = [],$posicion){
       $url = $this->getEndpoint();
       if($posicion == 1){
           foreach($params as $param){
                $url = sprintf($url,$param);
            } 
        } elseif($posicion == 2){
            $count = 0;
            foreach($params as $variable => $valor){
                $prefix = !($count) ? '?' : '&';  
                $url .= $prefix.$variable.'='.$valor;
                $count ++;
            } 
        } 
    return $url;
   }

   public function addParamToken(){
    $token = $this->getEndpoint();
    return $token;
}

public function addParamAppKey(){
    $appKey = $this->getEndpoint();
    return $appKey;
}

}
       