<?php

namespace app\Helpers;

use Exception;

class MethodConfig {

    const CONFIG_FILE = 'method.json';
    const CONFIG_PATH = ''; 

    private $configFile;
    private $configPath;
    private $configJson;
    private $name;
    private $method;
    private $enable;

    public function __construct($name = 'colocar', $configPath = null, $configFile = null){
        $this->setConfigPath($configPath ?? self::CONFIG_PATH)
             ->setConfigFile($configFile ?? self::CONFIG_FILE)
             ->setConfigJson(json_decode(file_get_contents($this->getConfigFile()), true)) 
             ->setName($name);
    }

    /** @param string configFile
    * @return $this
    */  
    public function setConfigFile($configFile){
        $this->configFile = $configFile; 
       return $this;  
    }

   /** @return string */
    public function getConfigFile(){
        return $this->getConfigPath().'/'.$this->configFile;  
    }

   /** @param string configPath
   * @return $this
   */
   public function setConfigPath($configPath){
      $this->configPath = base_path().$configPath; 
      return $this;  
   }

   /** @return string */
   public function getConfigPath(){
       return $this->configPath;  
   }

   /** @param string configJson
   * @return $this
   */
   public function setConfigJson($configJson){
       $this->configJson=$configJson;
       return  $this;  
   }
 
   /** @return string */
   public function getConfigJson(){
       return $this->configJson;  
   }

    /** @param string name
     * @return $this
     */
    public function setName($name){

        $config = array_filter($this->getConfigJson(), function($nodo) use($name) {return $nodo['name'] == $name; } );
        if(count($config)  != 1){
            throw new Exception('Enpoint not config found');
        }
        $this->setMethod($config[0]['method'])
             ->setEnable($config[0]['enable']);

        $this->name=$name;
        return  $this;  
   }
 
   /** @return string */
    public function getName(){
        return $this->name;  
    }

        
      /** @param string method
     * @return $this
     */
    public function setMethod($method){
        $this->method=$method;
        return $this;  
   }
 
   /** @return string */
    public function getMethod(){
        return $this->method;  
    }

    
    /** @param string enable
    * @return $this
    */
    public function setEnable($enable){
       $this->enable=$enable;
       return  $this;  
   }

   /** @return string */
    public function isEnable(){
        return $this->enable;  
    }

}