<?php

namespace app\Helpers;
use Exception;
use App\Helpers\ConfigJson;

class UrlConfigVtex extends ConfigJson{
     
  const CONFIG_FILE = 'conexion.json';

     
     private $account;
     private $url;
     private $token;
     private $appKey;

     public function __construct($account = 'principal', $configPath = null, $configFile = null){
         $this->setConfigPath($configPath ?? self::CONFIG_PATH)
              ->setConfigFile($configFile ?? self::CONFIG_FILE)
              ->setConfigJson(json_decode(file_get_contents($this->getConfigFile()), true)) 
              ->setAccount($account);
              
     }
      

    /** @param string account
      * @return $this
      */
      public function setAccount($account){
   
        $config = array_filter($this->getConfigJson(), function($nodo) use($account) {return $nodo['enviroment'] == $account; } );

        if(count($config,1) == 0){
            throw new Exception('Enviroment not config found');
        }
        $config=reset($config);

        $this->setUrl($config['url']) 
             ->setToken($config['token'])
             ->setAppKey($config['app_key'])
             ->setEnable($config['enable']);
        $this->account=$account;  
        return $this;  
   }

   /** @return string */
    public function getAccount(){
        return $this->account;  
    }

        /** @param string url
      * @return $this
      */
      public function setUrl($url){
         $this->url=$url; 
        return  $this;  
   }

   /** @return string */
    public function getUrl(){
        return $this->url;  
    }

    /** @param string token
      * @return $this
      */
      public function setToken($token){
        $this->token=$token;
        return  $this;  
   }

   /** @return string */
    public function getToken(){
        return $this->token;  
    }

    /** @param string appKey
      * @return $this
      */
      public function setAppKey($appKey){
        $this->appKey=$appKey;
        return  $this;  
   }

   /** @return string */
    public function getAppKey(){
        return $this->appKey;  
    }

 }

