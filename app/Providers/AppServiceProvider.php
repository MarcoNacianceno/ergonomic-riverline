<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       
    $this->app->singleton('GuzzleHttp\Client', function() {
            return new Client([
                'base_uri' => 'https://ergonomicmx.myvtex.com/',
                'headers' =>['content-type'=>'application/json','Accept'=>'application/json', 'X-VTEX-API-AppToken'=> 'ZIANHUJGYUFADEITSLQFINYMCOPINQOXIHOJGSPFKJZNWOFDMDDNWFXLKOLTFBSIGIRVQYMAVKVYOLPNILWJNABCKCZSGVAJQQHOXKNNRCRLKZXYYYLTFJVFGOKNYUUZ','X-VTEX-API-AppKey'=> 'vtexappkey-ergonomicmx-EGYQJW'],
                'http_errors' => true
            ]);
      });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
