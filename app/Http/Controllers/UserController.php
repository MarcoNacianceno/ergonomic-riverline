<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use App\Helpers\UrlConfigVtex;
use App\Helpers\DaxendpointConfig;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response('Metodo no permitido', 400);
    }

    public function getuserdax(Client $client,Request $request){ 

    if($request->input('email')){
        try{
           $configBaseUrl = new UrlConfigVtex();
           $configEndpoint = new DaxendpointConfig();

           $configEndpoint->setName('consultar_cliente');
           $configBaseUrl->setAccount('dax');
            
            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'APIKey' => $configBaseUrl->getAppKey()
            ])->get($configEndpoint->getEndpoint(),[
                'email' => $request->input('email')
            ]);

            if ($response->getStatusCode() == 200) {
                $getresponse = json_decode($response->getBody()->getContents(),true); 
                return response()->json($getresponse);
            } 
            if ($response->getStatusCode() == 400) {
                $getresponse = json_decode($response->getBody()->getContents(),true); 
                return response('No existe el cliente', 404);
            } 
        }
        catch(RequestException $e){
            
            return response('No existe el pedido', 404);
            $error['error'] = $e->getMessage();
            $error['request'] = $e->getRequest();
            if($e->hasResponse()){
                if ($e->getResponse()->getStatusCode() == '400'){
                    $error['response'] = $e->getResponse();
                    return response()->json($error['response']);
                }
            }
            echo print_r($error);
            Log::error('Error occurred in get request.', ['error' => $error]);
        }catch(Exception $e){
            return response('No existe el cliente', 404);
                    
        }

    } else {
        return response('Falta email', 400);
    }

    }

}
