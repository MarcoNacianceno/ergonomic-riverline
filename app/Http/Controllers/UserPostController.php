<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use app\Helpers\UrlConfigVtex;
use app\Helpers\EndpointConfig;

class UserPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response('Metodo no permitido', 400);
    }

    public function userpostdax(Client $client){
      try{
            $configBaseUrl = new UrlConfigVtex();
            $configEndpoint = new EndpointConfig(); 
            $response = $client->request('POST',$configBaseUrl->getUrl().$configEndpoint->getEndpoint(),[
            'firstName' => 'firstName',
            'lastName' => 'lastName',
            'birthDate' => 'birthDate',
            'email' => 'email',
            'corporateName' => 'corporateName',
            'idNumber' => 'idNumber',
            'phoneNumber' => 'phoneNumber',
            'daxId' => 'daxId',
            'b2bUser' => 'b2bUser'
        ]);  

        if ($response->getStatusCode() == 200) {
               $getresponse = json_decode($response->getBody()->getContents(),true); 
               dd($getresponse);
          } 
      }
      catch(RequestException $e){
     
         $error['error'] = $e->getMessage();
         $error['request'] = $e->getRequest();
         if($e->hasResponse()){
             if ($e->getResponse()->getStatusCode() == '400'){
                 $error['response'] = $e->getResponse(); 
             }
         }
         Log::error('Error occurred in get request.', ['error' => $error]);
      }catch(Exception $e){
        
         
      }
    }
}
