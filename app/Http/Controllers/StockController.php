<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;
use GuzzleHttp\Client;
class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response('Metodo no permitido', 400);
    }

    public function updatestock(Client $client)
    { 

    try{
       
        $response = $client->request('POST','api/logistics/pvt/inventory/skus/skuId/warehouses/{warehouseId}');  

          if ($response->getStatusCode() == 200) {
               $getresponse = json_decode($response->getBody()->getContents(),true); 
               dd($getresponse);
          } 
      }
      catch(RequestException $e){
     
         $error['error'] = $e->getMessage();
         $error['request'] = $e->getRequest();
         if($e->hasResponse()){
             if ($e->getResponse()->getStatusCode() == '400'){
                 $error['response'] = $e->getResponse(); 
             }
         }
         Log::error('Error occurred in get request.', ['error' => $error]);
      }catch(Exception $e){
        
         
      }
    
    }

}
