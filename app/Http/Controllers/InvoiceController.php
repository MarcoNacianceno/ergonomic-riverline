<?php
//https://DOMAINNAME/api/invoiceorderdax
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;
use GuzzleHttp\Client;
use App\Helpers\UrlConfigVtex;
use App\Helpers\EndpointConfig;
use Illuminate\Support\Facades\Validator;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response('Metodo no permitido', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response('Metodo no permitido', 400);
    }

    public function invoiceorderdax(Client $client, Request $request){

        echo "asñlkmdlksdla";

        $validator = Validator::make($request->all(), [
            'orderId' => 'required',
            'type' => 'required',
            'invoiceNumber' => 'required',
            'courier' => 'required',
            'trackingNumber' => 'required',
            'trackingUrl' => 'required',
            'items' => 'required',
            'issuanceDate' => 'required',
            'invoiceValue' => 'required'
        ]);

        if ($validator->fails()) {
            $data = [
                'succes' => false,
                'orderId' => $request->input('orderId')
            ];
            return response()->json($data, 400);
        }


        

      try{ 
        $configBaseUrl = new UrlConfigVtex();
        $configEndpoint = new EndpointConfig(); 

        $configEndpoint->setName('invoice_controller');

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'X-VTEX-API-AppToken' => $configBaseUrl->getToken(),
            'X-VTEX-API-AppKey' => $configBaseUrl->getAppKey()
        ])->post(sprintf($configEndpoint->getEndpoint(),$request->input('orderId')),[
            "type" => $request->input('type'),
            "invoiceNumber" => $request->input('invoiceNumber'),
            "courier" => $request->input('courier'),
            "trackingNumber" => $request->input('trackingNumber'),
            "trackingUrl" => $request->input('trackingUrl'),
            "items" => $request->input('items'),
            "issuanceDate" => $request->input('issuanceDate'),
            "invoiceValue" => $request->input('invoiceValue')
        ]);

        if ($response->getStatusCode() == 200) {
            $getresponse = json_decode($response->getBody()->getContents(),true); 
            $data = [
                'succes' => true,
                'orderId' => $request->input('orderId'),
                "type" => $request->input('type'),
                "invoiceNumber" => $request->input('invoiceNumber'),
                "courier" => $request->input('courier'),
                "trackingNumber" => $request->input('trackingNumber'),
                "trackingUrl" => $request->input('trackingUrl'),
                "items" => $request->input('items'),
                "issuanceDate" => $request->input('issuanceDate'),
                "invoiceValue" => ($request->input('invoiceValue')*100)  
            ];
            return response()->json($data);

            } 
        if ($response->getStatusCode() == 404) {
            $getresponse = json_decode($response->getBody()->getContents(),true); 
            $data = [
                'succes' => false,
                'orderId' => $request->input('orderId')
            ];
            return response()->json($data, 404);
    
            }
        if ($response->getStatusCode() == 409) {
            $getresponse = json_decode($response->getBody()->getContents(),true); 
            $data = [
                'succes' => false,
                'orderId' => $request->input('orderId')  
            ];
            return response()->json($data, 409);
    
            }
        }
        catch(RequestException $e){
        
            $error['error'] = $e->getMessage();
            $error['request'] = $e->getRequest();
            if($e->hasResponse()){
                if ($e->getResponse()->getStatusCode() == '400'){
                    $error['response'] = $e->getResponse(); 
                }
            }
            Log::error('Error occurred in get request.', ['error' => $error]);
        }catch(Exception $e){
       }
    }
}
