<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVtexOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vtex_orders', function (Blueprint $table) {
            $table->id();
            $table->integer('OrderId');
            $table->string('status');
            $table->string('order-created');
            $table->string('payment-approved');
            $table->string('authorize-fulfillment');
            $table->string('ready-for-handling');
            $table->string('start-handling');
            $table->string('invoice');
            $table->string('invoiced');
            $table->string('cancel');
            $table->string('canceled');
            $table->timestamps('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vtex_orders');
    }
}
