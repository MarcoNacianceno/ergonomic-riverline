<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use app\Helpers\placeorder;
use app\Http\Controllers\CancelController;
use app\Http\Controllers\InvoiceController;
use app\Http\Controllers\HandlingController;
use app\Http\Controllers\UserController;
use app\Http\Controllers\UserPostController;
use app\Http\Controllers\StockController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/cancelorderdax','CancelController@cancelorderdax'); 
Route::post('/invoiceorderdax','InvoiceController@invoiceorderdax'); 
Route::post('/handlingorderdax','HandlingController@handlingorderdax'); 
Route::post('/userpostdax','UserPostController@userpostdax'); 
Route::get('/getuserdax','UserController@getuserdax'); 
Route::post('/updatestock','StockController@updatestock'); 
